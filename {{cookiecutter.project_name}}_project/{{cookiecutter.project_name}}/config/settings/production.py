from .base import *

DEBUG = False

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{cookiecutter.project_name}}',
        'USER': '{{cookiecutter.project_name}}',
        'PASSWORD': '{{cookiecutter.project_name}}',
        'HOST': 'localhost',
        'PORT': ''
    }
}
